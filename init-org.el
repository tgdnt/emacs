;;; init-org.el ---  Org-mode settings
;;; Commentary:
;;; Separate file just for org settings, because it's just so big.

;;; Code:
(use-package org
  :commands (org-mode)
  :hook (org-mode . auto-fill-mode)
  :config (require 'org-crypt)
          (org-crypt-use-before-save-magic)
          (require 'org-id)
          ;; Snippet obtained from https://emacs.stackexchange.com/a/62089
          ;; to enable priority inheritance.
          (defun my/org-inherited-priority (s)
          (cond

          ;; Priority cookie in this heading
          ((string-match org-priority-regexp s)
              (* 1000 (- org-priority-lowest
                      (org-priority-to-value (match-string 2 s)))))

          ;; No priority cookie, but already at highest level
          ((not (org-up-heading-safe))
              (* 1000 (- org-priority-lowest org-priority-default)))

          ;; Look for the parent's priority
          (t
              (my/org-inherited-priority (org-get-heading)))))

          (setq org-priority-get-priority-function #'my/org-inherited-priority)
  :custom (org-todo-keywords '((type "TODO(t)"
                                     "WAIT(w)"
                                     "PROJ(p)"
                                   "|" "DONE(d)")))
          (org-archive-location (concat org-directory "archive/archive.org::"))
          (org-archive-save-context-info '(time file olpath ltags itags category))
          (org-stuck-projects '("TODO=\"PROJ\"" ("TODO") nil ""))
          (org-tags-column 2)
          (org-adapt-indentation nil)
          (org-clock-ask-before-exiting nil)
          (org-clock-report-include-clocking-task t)
          (org-duration-format 'h:mm)
          (org-startup-folded t)
          (org-startup-indented nil)
          (org-priority-start-cycle-with-default nil)
          (org-id-link-to-org-use-id 'create-if-interactive)
          (org-enforce-todo-dependencies t)
          (org-attach-id-dir (concat org-directory "attachments"))
          (org-refile-targets '((nil :maxlevel . 2)
                                (org-agenda-files :maxlevel . 2)))
          (org-id-link-to-org-use-id 'use-existing)
          (org-deadline-warning-days 7)
          ;; org-crypt
          (org-crypt-disable-auto-save t)
          (org-tags-exclude-from-inheritance '("crypt"))
          (org-crypt-tag-matcher "crypt")
          (org-crypt-key "<crypt-key>")
          (org-crypt-disable-auto-save t)
          ;; org-cite and latex
          (org-latex-packages-alist '(("margin=1in" "geometry" nil)))
          (org-latex-toc-command nil)
          (org-cite-global-bibliography '("~/everything/org/biblio.bib"))
          ;; org-agenda
          (org-agenda-files '("todo.org"))
          (org-agenda-custom-commands
                '(("A" "Agenda and Unscheduled TODOs"
                   ((agenda "")
                    ;; (todo "NEXT"
                    ;;       ((org-agenda-overriding-header "Next actions:")
                    ;;        (org-agenda-prefix-format '((todo . "%b ")))))
                    (todo "WAIT"
                          ((org-agenda-overriding-header "Waiting:")))
                    (todo "TODO"
                          ((org-agenda-overriding-header "Unscheduled:"))))
                   ((org-agenda-todo-ignore-scheduled t)
                    (org-agenda-todo-ignore-deadlines 'near)
                    (org-agenda-dim-blocked-tasks 'invisible)
                    (org-agenda-start-day nil)
                    (org-agenda-start-on-weekday nil)
                    (org-agenda-span 7)
                    (org-agenda-skip-deadline-prewarning-if-scheduled 'pre-scheduled)
                    (org-agenda-prefix-format '((agenda . "%b %?-12t% s")
                                                (todo . "%b ")
                                                (tags . "  ")
                                                (search . "  ")))
                    (org-agenda-deadline-leaders '("!" "%1d" "‼"))
                    (org-agenda-scheduled-leaders '("#" "%1d"))
                    (org-agenda-use-time-grid nil)
                    (org-agenda-start-with-log-mode t)
                    (org-agenda-start-with-clockreport-mode nil)
                    (org-agenda-clockreport-parameter-plist '(:maxlevel 2 :block thisweek :hidefiles t :step day :stepskip0 t :link t)))
                            nil)))
  :bind ("C-x h" . visible-mode)
        ("C-c n a" . org-agenda)
        ("C-c t" . org-todo)
)

(use-package org-contrib
  :pin nongnu
  :after org)

;; make visible-mode automatic when entering insert mode
(use-package org-appear
  :ensure nil
  :after org
  :init (setq org-hide-emphasis-markers t)
  :hook (org-mode . org-appear-mode)
  :custom (org-appear-autolinks t))

(use-package evil-org
  :pin melpa
  :after org
  :hook (org-mode . (lambda () evil-org-mode))
  :config
  (require 'evil-org-agenda)
  (evil-org-agenda-set-keys))

(use-package mermaid-mode
  :pin melpa)

(use-package ob-mermaid
  :pin melpa)

(provide 'init-org)
;;; init-org.el ends here
