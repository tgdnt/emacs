;;; init-extra.el --- Extra init stuff
;;; Commentary:
;;; Extra packages that I'm not likely to install anywhere but my personal laptop

;;; Code:

;;;;;;;;;;;;;;;
;;; Writing ;;;
;;;;;;;;;;;;;;;

;; citations
(use-package citar
  :custom (org-cite-insert-processor 'citar)
          (org-cite-follow-processor 'citar)
          (org-cite-activate-processor 'citar)
          (citar-bibliography '("~/everything/org/biblio.bib"))
          (citar-notes-paths '("~/everything/org/references/"))
          (citar-file-note-extensions '("org"))
  :hook (LaTeX-mode . citar-capf-setup)
        (org-mode . citar-capf-setup)
  :bind (("C-c n b" . #'citar-open-notes)
          :map org-mode-map :package org
          ("C-c b" . #'org-cite-insert)))

;; better than docview, for pdf
(use-package pdf-tools
  :pin melpa ;; don't reinstall when package updates
  :mode  ("\\.pdf\\'" . pdf-view-mode)
  :config (setq-default pdf-view-display-size 'fit-page)
          (pdf-tools-install :no-query)
          (require 'pdf-occur))

;;;;;;;;;;;;;;
;;; Auctex ;;;
;;;;;;;;;;;;;;
(use-package tex
  :ensure auctex
  :config (setq TeX-data-directory (expand-file-name "straight/repos/auctex" user-emacs-directory)
                TeX-view-program-selection
                    '(((output-dvi has-no-display-manager) "dvi2tty")
                        ((output-dvi style-pstricks) "dvips and gv")
                        (output-dvi "xdvi")
                        (output-pdf "PDF Tools")
                        (output-html "xdg-open"))
                TeX-check-path '("~/texlive/2024")))

(use-package evil-tex)

(use-package adaptive-wrap
  :pin gnu)

;;; END AUCTEX ;;;

;;;;;;;;;;;;;;;;;;;
;;; Programming ;;;
;;;;;;;;;;;;;;;;;;;

;; downloading and viewing Dash documentation files
(use-package dash-docs
  :pin melpa
  :init (defun elisp-doc ()
            (setq-local consult-dash-docsets '("Emacs Lisp")))
        (add-hook 'emacs-lisp-mode-hook 'elisp-doc)
  :custom (dash-docs-docsets-path (expand-file-name "docsets" user-emacs-directory))
          (dash-docs-browser-func 'eww))

(use-package consult-dash
  :pin melpa
  :bind (("M-s d" . consult-dash))
  :after consult
  :config
  ;; Use the symbol at point as initial search term
  (consult-customize consult-dash :initial (thing-at-point 'symbol)))

;;; END ;;;

(provide 'init-extra)
;;; init-extra.el ends here
