;;; init-coding.el ---  The packages for coding

;;; Code:

;; Syntax checking
(use-package flycheck
  :diminish
  :init (global-flycheck-mode))

(use-package flycheck-popup-tip
  :after (flycheck)
  :hook (flycheck-mode-hook . flycheck-popup-tip-mode))

;; Web design
(use-package emmet-mode
  :hook (sgml-mode . emmet-mode) ;; Auto-start on any markup modes
        (css-mode . emmet-mode)) ;; enable Emmet's css abbreviation.

(use-package sass-mode)

(use-package web-mode)

;; Python
(use-package python-mode
  :disabled t
  :mode ("\\.py\\'" . python-mode)
  :interpreter ("python" . python-mode))

;; highlight todo items everywhere
(use-package hl-todo
  :custom (hl-todo-keyword-faces
             `(("FIXME" error bold)
             ("TODO" error bold)
             ("REVISIT" error bold)))
          (hl-todo-exclude-modes nil)
  :config (add-to-list 'hl-todo-include-modes 'org-mode)
  :init (global-hl-todo-mode))

;; git
(use-package magit)

(use-package git-timemachine)

;; rest APIs via org source block
(use-package ob-restclient
  :pin melpa)

(provide 'init-coding)
;; init-coding.el ends here
