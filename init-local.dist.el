;;; init-local.el --- Settings that may change in different machines

;;; Code:
(setq org-directory (expand-file-name "/home/dnt/everything/org/"))

(load-file (expand-file-name "init-org.el" user-emacs-directory))
(require 'init-org)

(load-file (expand-file-name "init-writing.el" user-emacs-directory))
(require 'init-writing)

(load-file (expand-file-name "init-coding.el" user-emacs-directory))
(require 'init-coding)

(load-file (expand-file-name "init-extra.el" user-emacs-directory))
(require 'init-extra)

(provide 'init-local)
