;;; init-writing.el ---  The packages for writing

;;; Code:

;; Focused writing mode
(use-package olivetti
  :hook (olivetti-mode . typewriter-mode-toggle)
  :bind ("C-x C-w" . olivetti-mode)
  :custom (olivetti-body-width (+ fill-column 8))
  :config
      (defvar-local typewriter-mode nil
      "Typewriter mode, automatically scroll down to keep cursor in
      the middle of the screen. Setting this variable explicitly will
      not do anything, use typewriter-mode-on, typewriter-mode-off
      and typewriter-mode-toggle instead.")
      (defun typewriter-mode-on()
      "Automatically scroll down to keep cursor in the middle of screen."
        (interactive)
        (setq-local typewriter-mode t)
        (centered-cursor-mode +1))
      (defun typewriter-mode-off()
      "Automatically scroll down to keep cursor in the middle of screen."
        (interactive)
        (kill-local-variable 'typewriter-mode)
        (centered-cursor-mode -1))
      (defun typewriter-mode-toggle()
        "Toggle typewriter scrolling mode on and off."
        (interactive)
        (if typewriter-mode (typewriter-mode-off) (typewriter-mode-on))))

(use-package centered-cursor-mode
  :pin melpa)

;; Check for weasel words and some other simple rules
(use-package writegood-mode
  :bind ("C-c g" . writegood-mode))

;; spellchecking
(use-package flyspell-correct
  :after flyspell
  :bind (:map flyspell-mode-map
              ("C-;" . flyspell-correct-wrapper)))

;; show correction options in a popup instead of the minibuffer
(use-package flyspell-correct-popup
  :after (flyspell-correct))

;; WordNet-based Thesaurus
(use-package synosaurus
  :custom (synosaurus-choose-method 'default))

;; WordNet search and view
(use-package wordnut
  :pin melpa
  :bind ("C-c s" . wordnut-search))

;; fill and unfill with the same key
(use-package unfill
  :bind ("M-q" . unfill-toggle))

;; Markdown
(use-package markdown-mode)

(provide 'init-writing)
;;; init-writing.el ends here
